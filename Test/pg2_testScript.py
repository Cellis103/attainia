import unittest2
import HtmlTestRunner

from time import sleep
from TestBase.EnvironmentSetUp import EnvironmentSetup
from TestUtility.ScreenShot import SS
from pages.PageOpjects import page2
from pages.actions import Actions
from pages.locators import Locator


class test_pg2ColorValidation(EnvironmentSetup):

    def test_pg2ColorValidation(self):

        driver = self.driver
        self.driver.get(Locator.PAGE2_URL)
        self.driver.set_page_load_timeout(20)

        ss = SS(driver)
        pg2 = page2(driver)

        expected_title = "Ben's Attainia Project"
        expected_url_pg1 = "http://localhost:8000/usertables/page1/"
        expected_url_pg2 = "http://localhost:8000/usertables/page2/"

        try:
            if driver.title == expected_title:
                print("Webpage loaded successfully")
                self.assertEqual(driver.title, expected_title)
        except Exception as e:
            print(e + "Webpage Failed to load")

        try:
            if driver.current_url == expected_url_pg2:
                print("We're on page 2")
                self.assertEqual(driver.current_url, expected_url_pg2)
        except Exception as e:
            print(e, "Unknown page")

        print("Identifing Table")
        print(Locator.SEGMENT)

        rows = len(pg2.tableRow)
        cols = len(pg2.tableCol)

        print(rows, "table rows")
        print(cols, "table cols")

        pg2.highlightButton.click()
        sleep(5)

        # Save ScreenShot in corresponding path
        ss.ScreenShot(Locator.SSDIR_PG2 + Actions.SS_COLORVALIDATION_PG2 + Actions.SS_PNG)

        print(
            "ID" + "  |  " + "Username" + "  |  " + "Last Login" + "  |  " + "Login Count" + "  |  " + "Project Count")

        for row in range(2, rows + 1):
            for col in range(1, cols + 1):
                value = driver.find_element_by_xpath(
                    "/html/body/div/div/table/tr[" + str(row) + "]/td[" + str(col) + "]").text
                print(value, end='    ')
            print()

        print("Login Count")
        print(Locator.SEGMENT)

        for row in range(2, rows + 1):
            loginValue = driver.find_element_by_xpath(
                "/html/body/div/div/table/tr[" + str(row) + "]/td[4]").text
            print(loginValue, end='  ')

            if loginValue > "0" and pg2.getAfterBgColor().is_displayed() == True:
                print("Row is green")
            else:
                print("Row is white")
        print()

        print("Page Change")
        print(Locator.SEGMENT)
        pg2.pgChange.click()
        sleep(2)

        try:
            if driver.current_url == expected_url_pg1:
                print("We're on page 1")
                self.assertEqual(driver.current_url, expected_url_pg1)
        except Exception as e:
            print(e, "Unknown page")


if __name__ == '__main__':
    unittest2.main(testRunner=HtmlTestRunner.HTMLTestRunner(output=Locator.SLDIR_PG2))
