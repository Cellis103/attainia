import unittest2
import datetime
from selenium import webdriver
from pages.locators import Locator


class EnvironmentSetup(unittest2.TestCase):

    # Setup contains the browser setup attributes
    def setUp(self):
        self.driver = webdriver.Chrome(Locator.CHROME)
        print("Run Started at: " + str(datetime.datetime.now()))
        print("Chrome Environment Set Up")
        print(Locator.SEGMENT)
        self.driver.implicitly_wait(20)
        self.driver.maximize_window()

    # Teardown method just to close all the browser instances and then quit
    def tearDown(self):
        if (self.driver is not None):
            print(Locator.SEGMENT)
            print("Test Environment Destroyed")
            print("Run Completed at: " + str(datetime.datetime.now()))
            self.driver.close()
            self.driver.quit()
