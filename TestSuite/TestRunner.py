from unittest import TestLoader, TestSuite, TextTestRunner
from Test.pg1_testScript import test_pg1_ColorValidation
from Test.pg2_testScript import test_pg2ColorValidation

import testtools as testtools

if __name__ == "__main__":

    loader = TestLoader()
    suite = TestSuite((
        loader.loadTestsFromTestCase(test_pg1_ColorValidation),
        loader.loadTestsFromTestCase(test_pg2ColorValidation)
    ))

    runner = TextTestRunner(verbosity=2)
    runner.run(suite)

    concurrent_suite = testtools.ConcurrentStreamTestSuite(lambda: ((case, None) for case in suite))
    concurrent_suite.run(testtools.StreamResult())
