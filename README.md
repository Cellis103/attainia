First off, thank you for taking the time to review this code.
Although I have a good deal of expirance using Selenium, I have not used it with python, so it was a exciting challenge.

I created two test pages that validated title, urls, table existance, class existance,
button existance, and page navigation.

Upon futher abstraction these test could be broken down into separate pages, but I wasn't sure on when I should
turn in the project, and I didn't want to get myself stuck in a coding frenzy and ultimately forget to turn it in because of how
fun it was to make.

I separated my locators and my actions to make it easier to know what is doing what, it definitely could be cleaned up better.

One thing I noticed is the project is on the older side. Because of this, when the listing took stated the Django dependency 
it doesn't mention a version, so I did have a slight hicup getting things started because the newer versions of Django dosent 
use the same Syntax.

I created screen shot capturing for test validation and reporting purposes, I also had a htmltestrunner generating html reports
of the results of the test cases but somewhere down the line I broke it... (*cries*) but that is something that was fun to figure out

I also did an iteration through the table to get all values as text. The plan was to compare it to the json schema to validate the data values
were identical, but I kinda went down a rabbithole trying to fix the reporter. I did leave old samples of the html reports so you could see what it looked like
and, as I was pushing to git, I found a substitute that I will continue to try.

This project also made me appretiate how easy things become when you have unique identifiers. 

I did create a test suite so that when it comes to CICD we can just create a makefile to kickstart the testrunner and it would run all the test cases.

My dependencies were:

Django==2.2
html-testRunner==1.2.1
Jinja2==2.10.3
MarkupSafe==1.1.1
pytz==2019.3
selenium==3.141.0
sqlparse==0.3.0
urllib3==1.25.7
unittest2

Main Project:

https://github.com/Attainia/user-activity-code-challenge