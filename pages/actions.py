import datetime


class Actions(object):
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H-%M-%S")

    # --- ScreenShots ---
    SS_COLORVALIDATION_PG1 = "/pg1_ColorValidation_" + str(current_time) + ""
    SS_COLORVALIDATION_PG2 = "/pg2_ColorValidation_" + str(current_time) + ""
    SS_PAGECHANGE_PG1 = "/pg1_ChangeValidation_" + str(current_time) + ""
    SS_PAGECHANGE_PG2 = "/pg2_ChangeValidation_" + str(current_time) + ""
    SS_PNG = ".png"
    SS_JEPG = "jpeg"


    # --- Row Color Change ---
    #RED_CHANGE = "driver.find_element_by_css_selector('tr.redrow:nth-child(' + str(row) + ')')"
    #GREEN_CHANGE = "tr.greenrow:nth-child(3)"

     # --- Page Change ---
    PG1 = "/html/body/div/a"
    PG2 = "/html/body/div/a"