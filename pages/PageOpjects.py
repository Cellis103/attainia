from selenium.webdriver.common.by import By
from pages.locators import Locator
from pages.actions import Actions


class page1(object):

    def __init__(self, driver):
        self.driver = driver

        self.highlightButton = driver.find_element(By.CSS_SELECTOR, Locator.HIGHLIGHT_ROW_BUTTON)
        self.initialBgColor = driver.find_element(By.CSS_SELECTOR, Locator.WHITE_BG)
        self.afterBgColor = driver.find_element(By.CSS_SELECTOR, Locator.RED_BG)
        self.table = driver.find_element(By.XPATH, Locator.TABLE)
        self.tableCol = driver.find_elements(By.XPATH, Locator.TABLE_COLUMNS)
        self.tableRow = driver.find_elements(By.XPATH, Locator.TABLE_ROWS)
        self.pgChange = driver.find_element(By.XPATH, Actions.PG1)

    def getHighlightButton(self):
        return self.highlightButton

    def getInitialBgColor(self):
        return self.initialBgColor

    def getAfterBgColor(self):
        return self.afterBgColor

    def getTableCol(self):
        return self.tableCol

    def getTableRow(self):
        return self.tableRow


class page2(object):

    def __init__(self, driver):
        self.driver = driver

        self.highlightButton = driver.find_element(By.CSS_SELECTOR, Locator.HIGHLIGHT_ROW_BUTTON)
        self.initialBgColor = driver.find_element(By.CSS_SELECTOR, Locator.WHITE_BG)
        self.afterBgColor = driver.find_element(By.CSS_SELECTOR, Locator.GREEN_BG)
        self.table = driver.find_element(By.XPATH, Locator.TABLE)
        self.tableCol = driver.find_elements(By.XPATH, Locator.TABLE_COLUMNS)
        self.tableRow = driver.find_elements(By.XPATH, Locator.TABLE_ROWS)
        self.pgChange = driver.find_element(By.XPATH, Actions.PG2)

    def getHighlightButton(self):
        return self.highlightButton

    def getInitialBgColor(self):
        return self.initialBgColor

    def getAfterBgColor(self):
        return self.afterBgColor

    def getTableCol(self):
        return self.tableCol

    def getTableRow(self):
        return self.tableRow
