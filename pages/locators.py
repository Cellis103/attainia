
class Locator(object):
    # --- ScreenShots ---


    # --- Directories ---
    SSDIR_PG1 = "C:/Users/cynar/PycharmProjects/AttainiaAutomation/ScreenShots/page1"
    SSDIR_PG2 = "C:/Users/cynar/PycharmProjects/AttainiaAutomation/ScreenShots/page2"

    SLDIR_PG1 = "C:/Users/cynar/PycharmProjects/AttainiaAutomation/seleniumlogs/page1"
    SLDIR_PG2 = "C:/Users/cynar/PycharmProjects/AttainiaAutomation/seleniumlogs/page2"

    # --- WebDrivers ---
    CHROME = "C:/Users/cynar/PycharmProjects/AttainiaAutomation/drivers/chromedriver.exe"

    # --- Webpage locations ---
    PAGE1_URL = "http://localhost:8000/usertables/page1/"
    PAGE2_URL = "http://localhost:8000/usertables/page2/"

    # --- Separator ---
    SEGMENT = "---------------------------------------------------"

    # --- Page Locators ---
    HIGHLIGHT_ROW_BUTTON = "#app > div:nth-child(2) > button:nth-child(3)"
    WHITE_BG = "html body div#app div table tr"
    RED_BG = "#app > div > table > tr:nth-child(2)"
    GREEN_BG = "#app > div > table > tr:nth-child(6)"

    # --- Table ---
    TABLE = "/html/body/div/div/table"
    TABLE_COLUMNS = "/html/body/div/div/table/tr[1]/td"
    TABLE_ROWS = "/html/body/div/div/table/tr"
